function jump() {
  // If the player is on the ground, allow jumping
  if (onPlatform) {
    // Set the strength of the jump; -ve values mean up
    velocity = -10;
    onPlatform = false;
  }
};
// This function controls the animation
function updateMovement() {
  // store player jQuery object in a variable for convenience
  var player_m = $("#player_detect");
  // JUMP HANDLING
  if (!onPlatform) {
    // Update velocity if the player is in the air
    velocity += gravity;
    // Move player according to the new velocity
    var newPos = player_m.position().top + velocity + "px";
    player_m.css("top", newPos);
  }
  if (velocity >= 0) { // only detect platforms when falling
    onPlatform = false;
    $(".platform").each(function(index) {
      if (isCollisionBetween($(this), player_m) && player_m.position().top <= $(this).position().top) {
        player_m.css('top', $(this).position().top - player_m.height() + "px");
        onPlatform = true; // set player on a platform so that he can jump
        velocity = 0;
      }
    });
  }
  // MOVEMENT HANDLING
  if (jumpPressed) {
    jump();
  }
  // If the player goes left and is not on the left border allow moving
  if (leftPressed) {
    player_m.css("left", Math.max(0, player_m.position().left - speed ) + "px");
  }
  // If the player goes right and is not on the right border allow moving
  if (rightPressed) {
    player_m.css("left", Math.min(player_m.position().left + speed, $("#container").width() - player_m.width()) + "px");
  }
  if (upPressed) {
    // Make sure the player can move up before moving them
    if (player_m.position().top > speed) {
    //  player_m.css("top", player_m.position().top - speed + "px");
    } else {
      // If not, just put them on the left edge of the stage
      player_m.css("top", "0px");
    }
  }
  if (downPressed) {
    // Make sure the player can move down before moving them
    if (player_m.position().left > speed) {
      player_m.css("top", player_m.position().top + speed + "px");
    } else {
      // If not, just put them on the left edge of the stage
      player_m.css("top", $("#container").height() - player_m.height() + "px");
    }
  }
};
function isCollisionBetween(a, b) {
  var pos1 = $(a).position();
  var pos2 = $(b).position();
  return !(pos1.left > pos2.left + $(b).width() || pos2.left > pos1.left + $(a).width() || pos1.top > pos2.top + $(b).height() || pos2.top > pos1.top + $(a).height());
}