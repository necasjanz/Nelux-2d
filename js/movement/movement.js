// This function runs when the page is loaded
$(document).ready(function(e) {
  // Put the player in the top/middle of the screen
  // Calculate the player's "ground" position, which is the 
  // height of the stage minus the height of the player
  playerMaxTop = $("#container").height() - player_m.height();
  // Start the timer
  gameTimer = setInterval(update, refreshKeys);
  // Listen for keypresses
  $(document).keydown(function(e) {
    // If the spacebar is pressed, jump!
    if (e.which === SPACE) {
      jumpPressed = true;
    } else if (e.which === RIGHT) {
      rightPressed = true;
    } else if (e.which === LEFT) {
      leftPressed = true;
    } else if (e.which === UP) {
      upPressed = true;
    } else if (e.which === DOWN) {
      downPressed = true;
    } else if (e.which === SHIFT) {
      shiftPressed = true;
      speed = 20;
    } else if (e.which === DEBUG) {
      debugPressed = true;
    } else if (e.which === INVENTORY) {
      inventoryPressed= true;
      $( ".content" ).toggle();
    } else if (e.which === VAR) {
      varPressed= true;
      $( "#var1" ).toggle();
    }
  });
  $(document).keyup(function(e) {
    if (e.which === SPACE) {
      jumpPressed = false;
    } else if (e.which === RIGHT) {
      rightPressed = false;
    } else if (e.which === LEFT) {
      leftPressed = false;
    } else if (e.which === UP) {
      upPressed = false;
    } else if (e.which === DOWN) {
      downPressed = false;
    } else if (e.which === SHIFT) {
      shiftPressed = false;
      speed = 15;
    } else if (e.which === DEBUG) {
      shiftPressed = false;
    } else if (e.which === INVENTORY) {
      inventoryPressed = false;
    }else if (e.which === VAR) {
      varPressed = false;
    }
  });
    if (e.keyCode in map) {
        map[e.keyCode] = true;
               if (map[SHIFT] && map) {

           shiftPressed = true;
         //speed = 30;
        }
                else if (map[DEBUG] && map) {
                  debugPressed = true;
               printKeys();
        }
    }
}).keyup(function(e) {
    if (e.keyCode in map) {
        map[e.keyCode] = false;
    }
});