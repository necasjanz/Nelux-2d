  var player = $('#player_detect');
  var camera = $('#player_div'); 
  var playerLeft = parseInt( player.css('left') );
  var playerTop = parseInt( player.css('top') );
  var cameraLeft = parseInt( camera.css('left') );
  var cameraTop = parseInt( camera.css('top') );

  // make sure controlled box isn't touching an enemy
  if( cameraLeft != playerLeft ) {
    // check if controlled box is further to the right
    if( playerLeft > cameraLeft ) {
      camera.css({ 'transform':'translate(-50%,-50%) rotate(0deg)' });
      camera.animate( { left: '+=2' }, 0 );

    }
    else {
      camera.css({ 'transform':'translate(-50%,-50%)  rotateY(180deg)' });
      camera.animate( { left: '-=2' }, 0 );
    }
  }