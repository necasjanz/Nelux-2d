timeDay();
var placement = $('#base');
///////////////////////////Interface Resolution///////////
var GameInResX = 1600;//1600
var GameInResY = 768;//768
///////////////////////////Game borders(restraints)///////
var GameCoResX = 6200;//1280
var GameCoResY = 1600;//655
///////////////////////////Game size//////////////////////
var GameResX = 6200;//3200
var GameResY = 1600;//1600
///////////////////////////Player Camera//////////////////
var CviewX = 1280;//1280
var CviewY = 655;//655
//////////////////////////////////////////////////////////
var gameBGResX = GameResX;
var gameBGResY = GameResY;
var gameBGResCX = GameCoResX;
var gameBGResCY = GameCoResY;
/////////////////////////////////////////////////////////////Input variables////////////////////////////////////////////////////////////
var map = {87: false, 65: false, 83: false, 68: false, 16: false, 16: false, 192: false, 73: false, 18: false};
var LEFT = 65,  UP = 87,  RIGHT = 68, DOWN = 83, SPACE = 32, SHIFT = 16, DEBUG = 192, INVENTORY = 73; VAR = 18;// WASD
var leftPressed = false, rightPressed = false, upPressed = false, downPressed = false, shiftPressed = false, jumpPressed = false, debugPressed = false, inventoryPressed = false;
////////////////////////////////////////////////////////////Movement and colision variables/////////////////////////////////////////////
var player_m = $("#cene")
var detect = $('#player_detect');
var velocity = 0; // Player speed; +ve is down -ve is up
var gravity = 1; // Gravity amount
var playerMaxTop; // The top pixel when player hits ground
var timer; // The timer that controls the animation
var speed = 15; // left/right speed
var onPlatform = true;
var refreshScreen = 25;// Refresh scrolling rate
var refreshConditions = 1// Refresh conditions rate
var refreshKeys = 1// Refresh keypress rate
var refreshStairs = 1// Refresh stairs rate
// Use a variable for the player speed so it's easier to change
// Create a timer to trigger the update function repeatedly and move the player
/////////////////////////////////////////////////////////////////HTML propreties//////////////////////////////////////////////////////////
html = '' 
+ '<body>'
+ ''
+ '<div id="game_info1">'
+ '</div>'
+ '<div id="game_info2">'
+ '</div>'
+ '<div id="game_info3"></div>'
+ '<div id="container">'
+ '<div id="cene">'
+ '<div id="cenario">'
+ '<div id="room1"><div class="backDoor"><div class="door"></div></div></div>'
+ '<div id="room2"></div>'
+ '<div id="room3"></div>'
+ '<div id="room4"></div>'
+ '<div id="room5"></div>'
+ '<div id="room6"></div>'
+ '<div id="ground" class="platform"></div>'
+ '<div id="pillar" class="wall"></div>'
+ '<div id="underground" class="platform"></div>'
+ '<div class="detect"></div>'
//////////////////////////////////////////////////////////////////Caracter///////////////////////////////////////////////////////////////
+ '<div id="player_detect">'
+ '<div class="health"></div>'
+ '<div id="player_div"></div></div>'
+'</div></div></div></body>';
placement.html(html);
///////////////////////////////////////////////////////////////////Tree/////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////CSS Propreties/////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////Game Interface/////////////////////////////////////////////////////////////
placement.css({  'position': 'absolute','overflow': 'hidden','background-color': '','height': GameInResY, 'width': GameInResX });
$("#game_info1").css({  'position': 'absolute','overflow': 'hidden','background-color': 'black','height': '50%', 'width': '20%','right': '0%','top':'0%' });
$("#game_info2").css({  'position': 'absolute','overflow': 'hidden','background-color': 'blue','height': '70%', 'width': '20%','right': '0%','top':'50%' });
$("#game_info3").css({  'position': 'absolute','overflow': 'hidden','background-color': 'green','height': '15%', 'width': '80%','left': '0%','top':'85%' });
//////////////////////////////////////////////////////////////Game Cene//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////Game Content///////////////////////////////////////////////////////////////
$("#room1").css({  'position': 'absolute','overflow': 'hidden','background-color': 'white','height': '25%', 'width': '25%','left': '50%','top':'50%' });
$("#room2").css({  'position': 'absolute','overflow': 'hidden','background-color': 'blue','height': '25%', 'width': '25%','right': '0%','top':'50%' });
$("#room3").css({  'position': 'absolute','overflow': 'hidden','background-color': 'green','height': '25%', 'width': '25%','right': '0%','top':'25%' });
$("#room4").css({  'position': 'absolute','overflow': 'hidden','background-color': 'brown','height': '25%', 'width': '25%','right': '25%','top':'25%' });
$("#room5").css({  'position': 'absolute','overflow': 'hidden','background-color': 'yellow','height': '25%', 'width': '25%','right': '0%','top':'75%' });
//////////////////////////////////////////////////////////////Tree///////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////Platform handling///////////////////////////////////////////////////////////
$(".platform").css({  'position': 'absolute','background-color': 'black','height': '12px' });
$(".onPlatform").css({  'background-color': 'green' });
$("#ground").css({  'position': 'absolute','background-color': 'black','height': '4px', 'width': '100%','right': '3.2%','top':'75%' });
$("#underground").css({  'position': 'absolute','background-color': 'orange','height': '4px', 'width': '100%','right': '0%','top':'100%' });
$(".wall").css({  'position': 'absolute','background-color': 'orange','height': '120px' });
$("#pillar").css({ 'background-color': 'orange','height': '400px', 'width': '5px','right': '50%','top':'50%' });
$("#pillar.hit").css({ 'background-color': 'red' });
//////////////////////////////////////////////////////////////Player design//////////////////////////////////////////////////////////////
$("#player_detect").css({  'position': 'absolute','z-index': '10', 'background-color': 'green','height': '1.5%', 'width': '1%','right': '45%','top':'74%' });
$("#player_div").css({  'position': 'absolute','z-index': '0','overflow': 'hidden','opacity': '0.3', 'background-color': '','height': CviewY, 'width': CviewX, 'transform':'translate(-50%,-70%)' });
//$(".detect").css({  'position': 'absolute','opacity': '0.3', 'height': '25%', 'width': '25%' });
///////////////////////////////////////////////////////////////////////Debug/////////////////////////////////////////////////////////////////////
var interval = setInterval(function() {
    updateMovement();
$("#container").css({  'position': 'absolute','z-index':'-10', 'background-color': 'blue','height': gameBGResCY, 'width': gameBGResCX,'left': '0%','top':'0%' });
$("#cenario").css({  'position': 'absolute','background-color': 'pink','height': gameBGResY, 'width': gameBGResX,'box-sizing': 'border-box','border:': '2px solid red' });
////////////////////////////////////////////////////////////////Minimap///////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  var player = $('#player_detect');
  var camera = $('#player_div'); 
        newPos = new Object();
        newPos.left = -camera.offset().left;
        newPos.top = -camera.offset().top;
        newPos.left = -camera.offset().left;
        newPos.top = -camera.offset().top;
        $("#container").offset(newPos);
function terminal() {
  var cene = $('#cene');
  var playerdiv = $('#player_detect');
  var camera = $('#player_div');
};
terminal();
}, refreshScreen);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function initBattle(){
  renderObjStais();
  renderChars();
  bindEvents();
  stairsobj();
}
$(document).ready(function(){
  initBattle();

});
